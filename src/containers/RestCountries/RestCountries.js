import React, {Component} from 'react';
import './RestCountries.css';

import axios from 'axios';
import CountriesList from "../../components/CountriesList/CountriesList";
import CountriesInfo from "../../components/CountriesInfo/CountriesInfo";

class RestCountries extends Component {
  state = {
    countriesList: [],
    currentCountry: null,
    currentCountryIndex: null,
  };

  getCountriesList = () => {
    axios.get('https://restcountries.eu/rest/v2/all?fields=name;alpha3Code').then(response => {
      this.setState({countriesList: response.data});
    });
  };

  getCountryInfo = (countryCode, countryIndex) => {
    axios.get('https://restcountries.eu/rest/v2/alpha/' + countryCode).then(response => {
      if (response.data.borders.length > 0) {
        axios.get('https://restcountries.eu/rest/v2/alpha?codes=' + response.data.borders.join(';')).then(countryBorders => {
          const borders = countryBorders.data.map(country => {
            return country.name;
          });

          const countryInfo = {
            ...response.data,
            borders
          };

          this.setState({currentCountry: countryInfo, currentCountryIndex: countryIndex});
        });
      } else {
        this.setState({currentCountry: response.data, currentCountryIndex: countryIndex});
      }
    });
  };

  componentDidMount() {
    this.getCountriesList();
  }

  render() {
    return (
      <div className="RestCountries">
        <div className="CountriesListBlock">
          <CountriesList
            countries={this.state.countriesList}
            getCountryInfo={this.getCountryInfo}
            currentCountry={this.state.currentCountryIndex}
          />
        </div>
        <div className="CountriesInfoBlock">
          <CountriesInfo data={this.state.currentCountry} />
        </div>
      </div>
    );
  }
}

export default RestCountries;