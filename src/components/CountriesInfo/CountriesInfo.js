import React, {Fragment} from 'react';
import './CountriesInfo.css';

const CountriesInfo = ({data}) => {
  let countryInfo;

  if (!data) {
    countryInfo = (<div>No country selected</div>);
  } else {
    const flagImage = {
      backgroundImage: "url(" + data.flag + ")",
      backgroundSize: "auto 80px",
      backgroundRepeat: "no-repeat",
      backgroundPosition: "100% 0"
    };

    const bordersList = data.borders.reduce((array, border) => {
      array.push(<li key={array.length}>{border}</li>);

      return array;
    }, []);

    const languagesList = data.languages.reduce((array, language) => {
      array.push(<li key={array.length}>{language.name}</li>);

      return array;
    }, []);

    const currenciesList = data.currencies.reduce((array, currency) => {
      array.push(<li key={array.length}>{currency.name}</li>);

      return array;
    }, []);

    countryInfo = (
      <div className="CountriesInfo" style={flagImage}>
        <h2>{data.name}</h2>
        <div>
          <p><b>Region:</b> {data.region}</p>
          <p><b>Subregion:</b> {data.subregion}</p>
          <p><b>Capital:</b> {data.capital}</p>
          <p><b>Population:</b> {data.population}</p>
          <p><b>Area:</b> {data.area}</p>
        </div>
        <h3>Currencies:</h3>
        <ul>
          {currenciesList}
        </ul>
        <h3>Languages:</h3>
        <ul>
          {languagesList}
        </ul>
        <h3>Borders with:</h3>
        <ul>
          {bordersList.length > 0 ? bordersList : (<li style={{"list-style": "none"}}>...nothing</li>)}
        </ul>
      </div>
    );

    // console.log(data);
  }

  return (
    <Fragment>
      {countryInfo}
    </Fragment>
  );
};

export default CountriesInfo;
