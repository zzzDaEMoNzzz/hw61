import React from 'react';
import './CountriesList.css';

const CountriesList = ({countries, getCountryInfo, currentCountry}) => {
  const countriesArray = countries.reduce((array, country, index) => {
    const classes = ['CountryItem'];
    if (index === currentCountry) classes.push('active');

    array.push(
      <li
        className={classes.join(' ')}
        key={index}
        onClick={() => getCountryInfo(country.alpha3Code, index)}
      >
        {country.name}
      </li>
    );

    return array;
  }, []);

  return (
    <ul className="CountriesList">
      {countriesArray}
    </ul>
  );
};

export default CountriesList;
